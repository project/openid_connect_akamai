OPENID CONNECT AKAMAI(JANRAIN)
==============================

This module is a Akamai(Janrain) client plugin for OpenID Connect module.

The module extends the OpenID Connect by a proof key for code exchange  (PKCE)  authorization Code flow. See more details at https://oauth.net/2/pkce/

See more details of the Akamai implementation for the Open ID protocol by the link:
https://learn.akamai.com/en-us/webhelp/enterprise-application-access/enterprise-application-access/GUID-922796EA-AFC5-41B9-8523-7DC6E68A29FC.html

Supported by Akamai OpenID Connect specifications can be found by the link:
https://learn.akamai.com/en-us/webhelp/enterprise-application-access/enterprise-application-access/GUID-BA8CF701-26E3-4153-9D97-C4F2A8E658C4.html

Setup
-----

* Install this module.
* Visit the OpenID Connect config page: admin/config/services/openid-connect.
* Akamai(Janrain) will be available as a client.

Requirements
------------

* Drupal OpenID Connect module