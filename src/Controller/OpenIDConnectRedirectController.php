<?php

namespace Drupal\openid_connect_akamai\Controller;

use Drupal\openid_connect\Controller\OpenIDConnectRedirectController as BaseOpenIDConnectRedirectController;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Class OpenIDConnectRedirectController.
 *
 * @package Drupal\openid_connect\Controller
 */
class OpenIDConnectRedirectController extends BaseOpenIDConnectRedirectController {

  /**
   * Redirect.
   *
   * @param string $client_name
   *   The client name.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response starting the authentication request.
   */
  public function authenticate($client_name) {
    $response = parent::authenticate($client_name);

    $configuration = $this->config('openid_connect.settings.akamai')->get('settings');

    $query = $this->requestStack->getCurrentRequest()->query;
    if ($query->get('error')) {
      // Set cookie which controls that the login attempt using the SSO failed.
      $response->headers->setCookie(new Cookie('akamai_error', '1', time() + $configuration['sso_cookie_age']));
      if ($query->get('error') == 'login_required') {
        // Remove error message.
        $this->messenger()->deleteByType('warning');
      }

    }

    if ($this->currentUser()->isAuthenticated()) {
      $response->headers->setCookie(new Cookie('akamai_error', '1', 1));
    }

    return $response;
  }

}
