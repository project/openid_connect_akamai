<?php

namespace Drupal\openid_connect_akamai\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\openid_connect_akamai\Controller\OpenIDConnectRedirectController;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The OpenId Client Akamai settings.
   *
   * @var array
   */
  protected $akamaiSettings;

  /**
   * RouteSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config) {
    $this->akamaiSettings = $config->get('openid_connect.settings.akamai')->get('settings');
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($this->akamaiSettings['enable_sso'] && $route = $collection->get('openid_connect.redirect_controller_redirect')) {
      $route->setDefault('_controller', OpenIDConnectRedirectController::class . '::authenticate');
    }
  }

}
