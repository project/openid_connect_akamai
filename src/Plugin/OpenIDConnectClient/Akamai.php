<?php

namespace Drupal\openid_connect_akamai\Plugin\OpenIDConnectClient;

use Drupal\Component\Utility\Random;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\openid_connect\OpenIDConnectStateToken;
use Drupal\openid_connect\Plugin\OpenIDConnectClientBase;
use Exception;

/**
 * Akamai OpenID Connect client.
 *
 * Implements OpenID Connect Client plugin for Akamai(Janrain).
 *
 * @OpenIDConnectClient(
 *   id = "akamai",
 *   label = @Translation("Akamai(Janrain)")
 * )
 */
class Akamai extends OpenIDConnectClientBase {

  /**
   * Using the SHA256 hashing function.
   */
  const CODE_CHALLENGE_METHOD = 'sha256';
  const CODE_CHALLENGE_METHOD_AKAMAI_NAME = 'S256';

  /**
   * The name of the session variable in which the "code verifier" is stored.
   */
  const CODE_VERIFIER_VARIABLE = 'akamai_auth_code_verifier';

  /**
   * Default email login.
   *
   * @var null
   */
  private $emailForAuthorize = NULL;

  /**
   * {@inheritdoc}
   */
  public function authorize($scope = 'openid email') {
    // Use Akamai specific authorisations.
    $language_none = \Drupal::languageManager()
      ->getLanguage(LanguageInterface::LANGCODE_NOT_APPLICABLE);
    $redirect_uri = Url::fromRoute(
      'openid_connect.redirect_controller_redirect',
      [
        'client_name' => $this->pluginId,
      ],
      [
        'absolute' => TRUE,
        'language' => $language_none,
      ]
    )->toString(TRUE);

    $url_options = [
      'query' => [
        'client_id' => $this->configuration['client_id'],
        'response_type' => 'code',
        'scope' => $scope,
        'redirect_uri' => $redirect_uri->getGeneratedUrl(),
        'state' => OpenIDConnectStateToken::create(),
        'code_challenge' => $this->getCodeChallenge(),
        'code_challenge_method' => self::CODE_CHALLENGE_METHOD_AKAMAI_NAME,
        'ui_locales' => $this->getLocale(),
      ],
    ];

    if ($claims = $this->getClaimsForUrl()) {
      $url_options['query']['claims'] = $claims;
    }

    if ($this->emailForAuthorize) {
      $url_options['query']['oid_email'] = $this->emailForAuthorize;
      $this->emailForAuthorize = NULL;
    }

    $endpoints = $this->getEndpoints();
    // Clear _GET['destination'] because we need to override it.
    $this->requestStack->getCurrentRequest()->query->remove('destination');
    $authorization_endpoint = Url::fromUri($endpoints['authorization'], $url_options)->toString(TRUE);

    $response = new TrustedRedirectResponse($authorization_endpoint->getGeneratedUrl());
    // We can't cache the response, since this will prevent the state to be
    // added to the session. The kill switch will prevent the page getting
    // cached for anonymous users when page cache is active.
    \Drupal::service('page_cache_kill_switch')->trigger();

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  private function getLocale(): string {
    $current_language_id = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $current_language_component = explode('-', $current_language_id, 2);
    if (isset($current_language_component[1])) {
      $current_language_component[1] = strtoupper($current_language_component[1]);
      $current_language_id = implode('-', $current_language_component);
    }
    return $current_language_id;
  }

  /**
   * {@inheritdoc}
   */
  protected function getClaimsForUrl() {
    $claims = [];

    $userinfo = explode(PHP_EOL, $this->configuration['userinfo']);
    $userinfo = array_map('trim', $userinfo);
    $userinfo = array_filter($userinfo, function ($value) {
      return !empty($value);
    });

    foreach ($userinfo as $userinfo_item) {
      $claims['userinfo'][$userinfo_item] = NULL;
    }

    $id_token = explode(PHP_EOL, $this->configuration['id_token']);
    $id_token = array_map('trim', $id_token);
    $id_token = array_filter($id_token, function ($value) {
      return !empty($value);
    });

    foreach ($id_token as $id_token_item) {
      $claims['id_token'][$id_token_item] = NULL;
    }

    return !empty($claims) ? json_encode($claims) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function authorizeByEmail($scopes, $email) {
    $this->emailForAuthorize = $email;
    return $this->authorize($scopes);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['domain_endpoint'] = [
      '#title' => $this->t('Domain Name For Endpoints'),
      '#description' => $this->t('Please provide a domain name for endpoints, example https://DomainName.com'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['domain_endpoint'],
      '#weight' => -10,
    ];

    $form['customer_id'] = [
      '#title' => $this->t('Customer ID'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['customer_id'],
    ];

    $form['client_secret']['#disabled'] = TRUE;
    $form['client_secret']['#type'] = 'hidden';
    $form['client_secret']['#required'] = FALSE;

    $form['enable_sso'] = [
      '#title' => $this->t('Enable SSO login'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['enable_sso'],
    ];

    $form['sso_cookie_age'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SSO login interval in seconds'),
      '#default_value' => $this->configuration['sso_cookie_age'] ?? 3600,
      '#size' => 8,
      '#description' => $this->t('The time interval between SSO login attempts, in seconds.Must be 60 seconds or greater.'),
    ];

    $form['sso_whitelist'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Routes without sso'),
      '#default_value' => $this->configuration['sso_whitelist'] ?? '',
      '#maxlength' => NULL,
      '#description' => $this->t('Drupal routes for which there will be no attempts to SSO login.Enter a line-separated list of routes.'),
    ];

    $form['userinfo'] = [
      '#type' => 'textarea',
      '#title' => 'Claims: userinfo',
      '#maxlength' => NULL,
      '#default_value' => implode(PHP_EOL, (array) $this->configuration['userinfo']),
      '#description' => $this->t('Enter a line-separated list of claims (ie "birthdate").'),
    ];

    $form['id_token'] = [
      '#type' => 'textarea',
      '#title' => 'Claims: id_token',
      '#maxlength' => NULL,
      '#default_value' => implode(PHP_EOL, (array) $this->configuration['id_token']),
      '#description' => $this->t('Enter a line-separated list of claims (ie "birthdate").'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoints() {
    $uri = trim($this->configuration['domain_endpoint'], '/ ') . '/' . $this->configuration['customer_id'];
    return [
      'authorization' => $uri . '/login/authorize',
      'token' => $uri . '/login/token',
      'userinfo' => $uri . '/profiles/oidc/userinfo',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveTokens($authorization_code) {
    // Exchange `code` for access token and ID token.
    $language_none = \Drupal::languageManager()
      ->getLanguage(LanguageInterface::LANGCODE_NOT_APPLICABLE);
    $redirect_uri = Url::fromRoute(
      'openid_connect.redirect_controller_redirect',
      [
        'client_name' => $this->pluginId,
      ],
      [
        'absolute' => TRUE,
        'language' => $language_none,
      ]
    )->toString();
    $endpoints = $this->getEndpoints();

    $request_options = [
      'form_params' => [
        'code' => $authorization_code,
        'client_id' => $this->configuration['client_id'],
        'client_secret' => $this->configuration['client_secret'],
        'redirect_uri' => $redirect_uri,
        'grant_type' => 'authorization_code',
        'code_verifier' => $this->getCodeVerifier(),
      ],
      'headers' => [
        'Accept' => 'application/json',
      ],
    ];

    /* @var \GuzzleHttp\ClientInterface $client */
    $client = $this->httpClient;
    try {
      $response = $client->post($endpoints['token'], $request_options);
      $response_data = json_decode((string) $response->getBody(), TRUE);

      // Expected result.
      $tokens = [
        'id_token' => isset($response_data['id_token']) ? $response_data['id_token'] : NULL,
        'access_token' => isset($response_data['access_token']) ? $response_data['access_token'] : NULL,
      ];
      if (array_key_exists('expires_in', $response_data)) {
        $tokens['expire'] = \Drupal::time()->getRequestTime() + $response_data['expires_in'];
      }
      if (array_key_exists('refresh_token', $response_data)) {
        $tokens['refresh_token'] = $response_data['refresh_token'];
      }
      return $tokens;
    }
    catch (Exception $e) {
      $variables = [
        '@message' => 'Could not retrieve tokens',
        '@error_message' => $e->getMessage(),
      ];
      $this->loggerFactory->get('openid_connect_' . $this->pluginId)
        ->error('@message. Details: @error_message', $variables);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveUserInfo($access_token) {
    $userinfo = parent::retrieveUserInfo($access_token);
    return $userinfo;
  }

  /**
   * Create a “code verifier” and Code Challenge(PKCE).
   *
   * Code verifier a random string of 43 to 128(126 for Drupal Generator)
   * characters.
   */
  protected function getCodeChallenge() {
    $random = new Random();
    $code_verifier = $random->name(mt_rand(43, 126));

    $session = \Drupal::request()->getSession();
    $session->set(self::CODE_VERIFIER_VARIABLE, $code_verifier);

    // Then base64-url encodes the hashed string
    // to create a “code challenge” string.
    $hash = strtoupper(hash(self::CODE_CHALLENGE_METHOD, $code_verifier));
    return str_replace(['+', '/', '='], ['-', '_', ''], base64_encode(pack('H*', $hash)));
  }

  /**
   * {@inheritdoc}
   */
  protected function getCodeVerifier() {
    $session = \Drupal::request()->getSession();
    return $session->get(self::CODE_VERIFIER_VARIABLE);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Provider label as array for StringTranslationTrait::t() argument.
    $provider = [
      '@provider' => $this->getPluginDefinition()['label'],
    ];

    // Get plugin setting values.
    $configuration = $form_state->getValues();

    // Whether a client ID is given.
    if (empty($configuration['client_id'])) {
      $form_state->setErrorByName('client_id', $this->t('The client ID is missing for @provider.', $provider));
    }
    // Whether a customer ID is given.
    if (empty($configuration['customer_id'])) {
      $form_state->setErrorByName('customer_id', $this->t('The customer ID is missing for @provider.', $provider));
    }

    // Whether a customer ID is given.
    if (empty($configuration['domain_endpoint'])) {
      $form_state->setErrorByName('domain_endpoint', $this->t('The Domain name for endpoint is missing for @provider.', $provider));
    }

    // SSO cookie age.
    $sso_cookie_age_validate = $this->timeoutValidate($configuration['sso_cookie_age']);
    if (!$sso_cookie_age_validate) {
      $form_state->setErrorByName('sso_cookie_age', $this->t('SSO Cookie age must be an integer greater than 60.'));
    }
  }

  /**
   * Validate SSO cookie age.
   *
   * @param int $timeout
   *   The sso cookie age value in seconds to validate.
   *
   * @return bool
   *   Return TRUE or FALSE
   */
  public function timeoutValidate($timeout) {
    $validate = TRUE;

    if (!is_numeric($timeout) || $timeout < 0 || ($timeout > 0 && $timeout < 60)) {
      $validate = FALSE;
    }
    return $validate;
  }

}
