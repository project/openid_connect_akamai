<?php

namespace Drupal\openid_connect_akamai\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\openid_connect\Plugin\OpenIDConnectClientManager;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Force redirect anonymous.
 *
 * Force redirect anonymous user requests from site
 * to Akamai login endpoint for SSO.
 */
class AnonymousRedirectSubscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * OpenId Akamai Client.
   *
   * @var \Drupal\openid_connect_akamai\Plugin\OpenIDConnectClient\Akamai
   */
  protected $openidAkamaiClient;

  /**
   * The OpenId Client Akamai settings.
   *
   * @var array
   */
  protected $akamaiSettings;

  /**
   * Guzzle Http Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a new AnonymousRedirectSubscriber instance.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Configuration object.
   * @param \Drupal\openid_connect\Plugin\OpenIDConnectClientManager $openid_client_manager
   *   OpenId Client Manager.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Guzzle Http Client.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(AccountInterface $current_user, ConfigFactoryInterface $config, OpenIDConnectClientManager $openid_client_manager, ClientInterface $http_client) {
    $this->currentUser = $current_user;
    $this->akamaiSettings = $config->get('openid_connect.settings.akamai')->get('settings');
    $this->openidAkamaiClient = $openid_client_manager->createInstance('akamai', $this->akamaiSettings);
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['redirectAnonymous', 100];

    return $events;
  }

  /**
   * Redirects anonymous users to login endpoint for SSO.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The request event.
   */
  public function redirectAnonymous(RequestEvent $event) {
    $request = $event->getRequest();

    if ($this->akamaiSettings['enable_sso']
      && $this->currentUser->isAnonymous()
      && !$request->cookies->has('akamai_error')
    ) {

      $url = Url::fromUserInput($request->getPathInfo());
      if ($url->isRouted() && $this->routeInWhiteList($url->getRouteName())) {
        return;
      }

      $uri = trim($this->akamaiSettings['domain_endpoint'], '/ ') . '/';
      $uri .= $this->akamaiSettings['customer_id'];
      $uri .= '/login/.well-known/openid-configuration';
      try {
        $this->httpClient->get($uri);
        // Expected result.
        // getBody() returns an instance of Psr\Http\Message\StreamInterface.
        // @see http://docs.guzzlephp.org/en/latest/psr7.html#body
        $response = $this->openidAkamaiClient->authorize();
        $response->setTrustedTargetUrl($response->getTargetUrl() . '&prompt=none');
        $event->setResponse($response);

        $_SESSION['openid_connect_destination'] = $request->getRequestUri();
      }
      catch (RequestException $e) {
      }
    }

  }

  /**
   * Check route in SSO white list.
   *
   * @param string $route_name
   *   Route name.
   *
   * @return bool
   *   The route isset in white list.
   */
  protected function routeInWhiteList(string $route_name) {
    $sso_whitelist = explode(PHP_EOL, $this->akamaiSettings['sso_whitelist']);
    $sso_whitelist = array_map('trim', $sso_whitelist);
    $sso_whitelist = array_filter($sso_whitelist, function ($value) {
      return !empty($value);
    });
    $sso_whitelist = array_merge($sso_whitelist, $this->getDefaultSsoWhiteList());

    $list = array_map(function ($line) {
      return str_replace('\*', '.*', preg_quote($line, '/'));
    }, $sso_whitelist);
    foreach ($list as $line) {
      if (preg_match('/^' . $line . '$/', $route_name)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Check route in SSO white list.
   */
  protected function getDefaultSsoWhiteList() {
    return [
      'user*',
      'openid_connect*',
    ];
  }

}
